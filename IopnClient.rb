#!/usr/bin/ruby
 
require "json"
require 'base64'
require 'openssl'

require_relative './HttpCurl'

class IopnClient

	@@headers = nil
    @@body = nil
    @@snsMessage = nil
    @@fields = {}
    @@signatureFields = {}
    @@certificate = nil
    @@expectedCnName = 'sns.amazonaws.com'
    @@defaultHostPattern = /^sns\.[a-zA-Z0-9\-]{3,}\.amazonaws\.com(\.cn)?$/    
    @@ipnConfig = {
    				'cabundle_file'  => nil,
                   	'proxy_host'     => nil,
                    'proxy_port'     => -1,
                    'proxy_username' => nil,
                   	'proxy_password' => nil
                }

    def initialize(headers, body, ipnConfig = nil)
        @@headers = headers.inject({}) do |headers, keys|
		 	headers[keys[0].downcase] = keys[1]
			headers
		end

        @@body = body.to_s
        if ! ipnConfig.nil?
            checkConfigKeys(ipnConfig)
        end
        # Get the list of fields that we are interested in
        @@fields = {
	            "Timestamp" => true,
	            "Message" => true,
	            "MessageId" => true,
	            "Subject" => false,
	            "TopicArn" => true,
	            "Type" => true
	        }
        # Validate the IPN message header [x-amz-sns-message-type]
        validateHeaders()
        # Converts the IPN [Message] to Notification object
        getMessage()
        # Checks if the notification [Type] is Notification and constructs the signature fields
        checkForCorrectMessageType()
        # Verifies the signature against the provided pem file in the IPN
        #constructAndVerifySignature()
    end

    

	# Setter function
    # Sets the value for the key if the key exists in ipnConfig    
    def __set(name, value)
        if @@ipnConfig.has_key?(name.to_s.downcase)
            	@@ipnConfig[name] = value
        else
            raise "Key #{name} is not part of the configuration"
        end
    end

    # Getter function
    # Returns the value for the key if the key exists in ipnConfig
    def __get(name)
    	if @@ipnConfig.has_key?(name)
	    	return @@ipnConfig[name]
        else 
            raise "Key #{name} was not found in the configuration"
        end
    end

    # Verify that the signature is correct for the given data and public key
    # @param string signatureFields            data to validate
    # @param string signature       decoded signature to compare against
    # @param string certificate     certificate object
    def verifySignatureIsCorrectFromCertificate(signature)

        certKey = OpenSSL::X509::Certificate.new(@@certificate).public_key
        if !certKey
            raise "Unable to extract public key from cert"
        end
        begin
            certInfo = OpenSSL::X509::Certificate.new(@@certificate)
            certSubject = certInfo.subject
            if certSubject.nil?
                raise "Error with certificate - subject cannot be found"
            end
        rescue Exception => e
            raise "Unable to verify certificate - error with the certificate subject"
        end
        if certInfo.subject.to_a.find { |name, _, _| name == 'CN' }[1] != @@expectedCnName
            raise "Unable to verify certificate issued by Amazon - error with certificate subject"
        end
        result = false
        begin
            sha1 = OpenSSL::Digest::SHA1.new
            pub_key   = OpenSSL::PKey::RSA.new(certKey)
            result = pub_key.verify(sha1, signature, @@signatureFields)
        rescue Exception => e
            raise "Unable to verify signature - error with the verification algorithm"
        end
        return result
    end

    # returnMessage() - JSON decode the raw [Message] portion of the IPN
    def returnMessage()
        begin
            return JSON.parse(@@snsMessage['Message'])
        rescue Exception => e
            begin
                #core ruby json parse handling
                @@snsMessage['Message']
            rescue Exception => e
                raise "Error with message - content is not in json format #{e}" 
            end
        end       
    end

    # toJson() - Converts IPN [Message] field to JSON
    # Has child elements
    # ['NotificationData'] [XML] - API call XML notification data
    # @param remainingFields - consists of remaining IPN array fields that are merged
    # Type - Notification
    # MessageId -  ID of the Notification
    # Topic ARN - Topic of the IPN
    # @return response in JSON format
    #
    
    def toJson()
        response = simpleXmlObject()
        # Merging the remaining fields with the response
        remainingFields = getRemainingIpnFields()
        responseArray = remainingFields.marge(response)
        # Converting to JSON format
        response = JSON.generate(responseArray)
        return response
    end

    # toArray() - Converts IPN [Message] field to hash
    # @return response in hash format
    def toArray()
        response = simpleXmlObject()
        # Merging the remaining fields with the response hash
        remainingFields = getRemainingIpnFields()
        response = remainingFields.merge(response)
        return response
    end

    # Verify that the signature is correct for the given data and public key
    # @param string signature       decoded signature to compare against
    # @param string certificatePath path to certificate, can be file or url
    # @raise Exception if there is an error with the call
    # @return bool true if valid
    def constructAndVerifySignature()
        signature       = Base64.decode64( getMandatoryField("Signature") )
        certificatePath = getMandatoryField("SigningCertURL")
        validateUrl(certificatePath)
        @@certificate = getCertificate(certificatePath)
        result = verifySignatureIsCorrectFromCertificate(signature)
        if ! result 
            #raise "Unable to match signature from remote server: signature of #{@@certificate} , SigningCertURL of #{certificatePath} , SignatureOf ".concat(getMandatoryField("Signature"))
        end
        return result
    end

    private            # subsequent methods will be 'private'
        def checkConfigKeys(ipnConfigs)
            ipnConfig = ipnConfigs.inject({}) do |ipnConfigs, keys|
                ipnConfigs[keys[0].downcase] = keys[1]
                ipnConfigs
            end
            ipnConfig = trimArray(ipnConfig)

            ipnConfig.each_pair do |key, value|
                if @@ipnConfig.has_key?(key)
                    @@ipnConfig[key] = value
                else
                    raise "Key #{key} is either not part of the configuration or has incorrect Key name.
                    check the ipnConfig array key names to match your key names of your config array"
                end
            end
        end
        
        # Trim the input hash key values 
        def trimArray(array)
        	array.each_pair do |key, value|
        		array[key] = value.strip
        	end
        	return array
        end
      
      	def validateHeaders()
            # Quickly check that this is a sns message
            if ! @@headers.has_key?('x-amz-sns-message-type')
            	raise "Error with message - header does not contain x-amz-sns-message-type header"
            end
            if @@headers['x-amz-sns-message-type'] != 'Notification'
                raise "Error with message - header x-amz-sns-message-type is not Notification, is #{headers['x-amz-sns-message-type']}"
            end
        end

        def getMessage()
        	begin            
    		 	@@snsMessage = JSON.parse(@@body)
    		rescue JSON::ParserError => e
                begin
                    # core ruby json handling
                    a =  @@body.gsub(/\\"/, '"').gsub(/"{/, '{').gsub(/\}"/, '}')
                    a = a.gsub('\\\\/', '/')
                    a = a.gsub('\\\\n', '\n')
                    @@snsMessage = JSON.parse(a)
                rescue
                    raise "Error with message - content is not in json format #{e}" 
                end
    		end

        end

        # Checks if the Field [Type] is set to ['Notification']
        # Gets the value for the fields marked true in the fields hash
        # Constructs the signature string
        def checkForCorrectMessageType()
            type = getMandatoryField("Type")
            if ! type.casecmp?("Notification")
                raise "Error with SNS Notification - unexpected message with Type of #{type}"
            end
            type = getMandatoryField("Type")
           	if ! type.eql?("Notification")
                raise "Error with signature verification - unable to verify #{type} message"
            else
                # Sort the fields into byte order based on the key name(A-Za-z)
                @@fields = @@fields.sort_by{ |key, val| key.to_s }.to_h
                # Extract the key value pairs and sort in byte order
                signatureFields = []
                @@fields.each_pair do |fieldName, mandatoryField|
                    if mandatoryField
                        value = getMandatoryField(fieldName)
                    else
                        value = getField(fieldName)
                    end
                    if ! value.nil?
                        signatureFields.push(fieldName)
                        signatureFields.push(value)
                    end
                end

                # Create the signature string - key / value in byte order delimited by newline character + ending with a new line character
                @@signatureFields = signatureFields.join("\n").concat("\n").gsub("=>",":").gsub(/", "/,'","').gsub(/<\//,'<\\/')
            end
        end 

        # Ensures that the URL of the certificate is one belonging to AWS.
        # @param string url Certificate URL
        # @raise InvalidSnsMessageException if the cert url is invalid.
        def validateUrl(url)
            parsed = URI.parse(url)
            if ( parsed.scheme.empty?  || parsed.host.empty? || parsed.scheme != 'https' || url[url.length-4,url.length] != '.pem' || ! @@defaultHostPattern === parsed.host )
                raise 'The certificate is located on an invalid domain.'
            end
        end

    	# gets the certificate from the certificatePath using Curl
        def getCertificate(certificatePath)        
            httpCurlRequest = HttpCurl.new(@@ipnConfig)
    		result = httpCurlRequest.httpGet(certificatePath, '')
    		response = result['response']
            return response
        end


        # Extract the mandatory field from the message and return the contents
        # @param string fieldName name of the field to extract
        # @raise Exception if not found
        # @return string field contents if found    
        def getMandatoryField(fieldName)
            value = getField(fieldName)
            if value.nil?
                raise "Error with json message - mandatory field #{fieldName} cannot be found"
            end
            return value
        end
        
        # Extract the field if present, return null if not defined
        # @param string fieldName name of the field to extract
        # @return string field contents if found, nil otherwise
        def getField(fieldName)
        	if @@snsMessage.has_key?(fieldName)
        		return @@snsMessage[fieldName]
        	else 
        		return nil
        	end
        end


        # addRemainingFields() - Add remaining fields to the datatype
        # Has child elements
        # ['NotificationData'] [XML] - API call XML response data
        # Convert to SimpleXML element object
        # Type - Notification
        # MessageId -  ID of the Notification
        # Topic ARN - Topic of the IPN
        # @return response in hash format
        def simpleXmlObject()
            ipnMessage = returnMessage()
          
            require 'rexml/document'
            doc = REXML::Document.new(ipnMessage['NotificationData'])
            require 'active_support/all'
            response = Hash.from_xml(doc.to_s)

            # Adding the Type, MessageId, TopicArn details of the IPN to the hash
            response['Type']      = @@snsMessage['Type']
            response['MessageId'] = @@snsMessage['MessageId']
            response['TopicArn']  = @@snsMessage['TopicArn']
            return response
        end


        # Gets the remaining fields of the IPN to be later appended to the return message
        def getRemainingIpnFields()
            ipnMessage = returnMessage()
            remainingFields ={
                                'NotificationReferenceId' =>ipnMessage['NotificationReferenceId'],
                                'NotificationType' =>		ipnMessage['NotificationType'],
                                'SellerId' =>				ipnMessage['SellerId'],
                                'ReleaseEnvironment' =>		ipnMessage['ReleaseEnvironment']
                            }
            return remainingFields
    	end
end