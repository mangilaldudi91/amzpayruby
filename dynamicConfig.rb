#!/usr/bin/ruby


require 'yaml'
require_relative './HttpCurl'


	def getDynamicConfig()
		filepath= File.dirname(__FILE__).concat("/config.yaml")
		config = YAML.load_file(filepath)
		if( config['lastFetchedTimeForFetchingConfig']<(Time.now.to_i-3600) )
			httpCurlRequest = HttpCurl.new()
			dynamicConfig = httpCurlRequest.httpGet('https://amazonpay.amazon.in/getDynamicConfig?key=serverSideSDKPHP', '')
			dynamicConfig = httpCurlRequest.httpGet('https://amazonpay.amazon.in/getDynamicConfig?key=serverSideSDKPHP', '')
			response = JSON.parse(dynamicConfig['response'])
			if  dynamicConfig.is_a?(Hash) && dynamicConfig['status_code'] == '200' && !response['publicKey'].empty?
				config['publicKey'] = response['publicKey']
				config['lastFetchedTimeForFetchingConfig'] = Time.now.to_i
				File.open(filepath, 'w') {|f| f.write(config.to_yaml) }
			else
				raise "Public key is not present"
			end
		end
	end
getDynamicConfig()