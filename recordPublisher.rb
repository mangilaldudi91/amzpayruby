#!/usr/bin/ruby


require 'yaml'
require "json"
require_relative './HttpCurl'



def postMetrics()

		metricsSize = getFileSize()
		path= File.dirname(__FILE__)
		config = YAML.load_file("#{path}/config.yaml")
		if( config['lastFetchedTimeForPostingMetrics']<(Time.now.to_i-300) || metricsSize > 500000 )
		data = {}
		post_data ={}
		data['latency'] = parseLatencyFile()
		data['count'] = parseCountFile()
		post_data['payload'] = data
		post_data['sdkType'] = 'ServerSideSDKPHP'
   		post_data = JSON.generate({'data' => post_data})
		httpCurlRequest = HttpCurl.new()
		response = httpCurlRequest.httpPost("http://amazonpay.amazon.in/publishMetrics/data=#{post_data}",'')
		#$response = $httpCurlRequest->httpPost('http://amazonpay.amazon.in/publishMetrics/data=$post_data', null, null);
		config[ 'lastFetchedTimeForPostingMetrics' ] = Time.now.to_i
		
		#File.open(path.concat("/config.yaml"), 'w') {|f| f.write(config.to_yaml) }

		if(File.exist?("#{path}/metrics/latencyMetrics.txt"))
			#File.delete("#{path}/metrics/latencyMetrics.txt")
		end
		if(File.exist?("#{path}/metrics/countMetrics.txt"))
			#File.delete("#{path}/metrics/countMetrics.txt")
		end
	end
end

def getFileSize()
	path= File.dirname(__FILE__)
	size = 0;
	if(File.exist?("#{path}/metrics/latencyMetrics.txt"))
		size = size + File.size("#{path}/metrics/latencyMetrics.txt")
	end
	if(File.exist?("#{path}/metrics/countMetrics.txt"))
		size = size + File.size("#{path}/metrics/countMetrics.txt")
	end
	return size
end

def preg_grep_keys( pattern, input, flags = 0 )
	#keys = preg_grep( pattern, input.keys ), flags )
	keys = input.keys.grep(pattern)
	vals = {}
	keys.each do |key| 
		vals[key] =  input[key]
	end
	return vals
end

def parseLatencyFile()
	path= File.dirname(__FILE__)
	pattern= /(?<operation>\w+)\s+(?<key>\w+)\s+(?<time>\d+(\.\d{1,6})?)/i
	if(File.exist?("#{path}/metrics/latencyMetrics.txt"))
		title = 'data'
		json_data= []
		File.open("#{path}/metrics/latencyMetrics.txt").each do |line|
			result = line.chomp.match(pattern)
			result = Hash[ result.names.zip( result.captures ) ]
			#preg_match($pattern,$line,$result);
			json_data.push(preg_grep_keys(/operation|key|time/,result))
		end
		return JSON.generate(json_data)
	else
		return nil
	end
end

def parseCountFile()
	path= File.dirname(__FILE__)
	pattern=/(?<key>\w+)\s+(?<count>\d+)/i
	if(File.exist?("#{path}/metrics/countMetrics.txt"))
		title = 'data'
		json_data=[]
		File.open("#{path}/metrics/countMetrics.txt").each do |line|
			result = line.chomp.match(pattern)
			result = Hash[ result.names.zip( result.captures ) ]
			#preg_match($pattern,$line,$result);
			json_data.push(preg_grep_keys(/key|count/,result))
		end
		return JSON.generate(json_data)
	else
		return nil
	end
end
postMetrics()