#!/usr/bin/ruby
require 'uri'
require "cgi"
require "time"
require 'openssl'
require 'base64'
require 'rexml/document'
require_relative './HttpCurl'

class PWAINBackendSDK
	# constructor method

	@@config = {
        'merchant_id' => nil,
        'secret_key' => nil,
        'access_key' => nil,
        'base_url'  => nil,
        'application_name' => nil,
        'application_version' => nil,
        'currency_code' => nil,
        'client_id' => nil,
        'region' => nil,
        'sandbox' => nil,
        'platform_id' => nil,
        'content_type' => nil
    }

   def initialize(configArray = {})
   		if ! configArray.to_s.empty?
      		checkConfigKeys(configArray)
      	else
      		raise 'config cannot be null.'
      	end

      	# Get the list of fields that we are interested in
		@params_SignAndEncrypt = {
			"orderTotalAmount" => true,
			"orderTotalCurrencyCode" => true,
			"sellerOrderId" => true,
			"customInformation" => false,
			"sellerNote" => false,
			"transactionTimeout" => false,
			"isSandbox" => false,
			"sellerStoreName" => false
		}
		@params_verifySignature = {
				"description" => true,
				"reasonCode" => true,
				"status" => true,
				"signature" => false,
				"sellerOrderId" => false,
				"amazonOrderId" => false,
				"transactionDate" => false,
				"orderTotalAmount" => false,
				"orderTotalCurrencyCode" => false,
				"customInformation" => false
		}
    end # end initialize method


	# generates the signature for the parameters given with the aws secret key
	# provided and encrypts parameters along with signature
	# @param  $parameters
	# @return encrypted payload
	def generateSignatureAndEncrypt(parameters = {})
		checkForRequiredParameters( parameters, @params_SignAndEncrypt )
		encryptedResponse = {}
		parameters = calculateSignForEncryption( parameters )
		parametersToEncrypt = getParametersToEncrypted( parameters )
		dataToEncrpyt = getParametersAsString( parametersToEncrypt )
		sessionKey = getSecureRandomKey()
		pubKey = getPublicKey()
		iv = getSecureRandomIv()
		key = OpenSSL::PKey::RSA.new(pubKey)
		crypted =  Base64.strict_encode64(key.public_encrypt(sessionKey,OpenSSL::PKey::RSA::PKCS1_OAEP_PADDING)).encode('UTF-8')
		encyptedData = encryptAndAppendTag( sessionKey, iv, dataToEncrpyt , nil )
		encyptedData = Base64.strict_encode64(encyptedData).encode('UTF-8')
		encryptedResponse['payload'] = CGI.escape(encyptedData).gsub("+", "%20")
		encryptedResponse['key'] = CGI.escape( crypted ).gsub("+", "%20")
		encryptedResponse['iv'] = CGI.escape( Base64.strict_encode64(iv).encode('UTF-8') ).gsub("+", "%20")
		encryptedResponseAsString = getParametersAsString( encryptedResponse )
		return encryptedResponseAsString
	end #end generateSignatureAndEncrypt method

	# calculates the signature for the parameters given with the aws secret key
	# provided and verfies it against the signature provided.
	# @param paymentResponseMap the paymentResponse Map containing the signature
	# @return true if the signature provided is valid
	def verifySignature(paymentResponseMap)
		validateNotEmpty( paymentResponseMap, "paymentResponseMap" )
		checkForRequiredParameters( paymentResponseMap, @params_verifySignature )
		paymentResponseMap["description"] = CGI::unescape(paymentResponseMap["description"])
		providedSignature = CGI.escape(paymentResponseMap ['signature']).gsub("+", "%20")
		paymentResponseMap.delete('signature')
		validateNotNull( providedSignature, "ProvidedSignature" )
		calculatedSignature = calculateSignForVerification( paymentResponseMap )
	    calculatedSignature =  CGI.escape(calculatedSignature['Signature']).gsub("+", "%20")
		return (calculatedSignature == providedSignature);
	end

	# Refund API call - Refunds a previously captured amount.
    # @see http://docs.developer.amazonservices.com/en_US/off_amazon_payments/OffAmazonPayments_Refund.html
    #
    # @param requestParameters['merchant_id'] - [String]
    # @param requestParameters['amazon_capture_id'] - [String]
    # @param requestParameters['refund_reference_id'] - [String]
    # @param requestParameters['refund_amount'] - [String]
    # @param requestParameters['currency_code'] - [String]
    # @optional requestParameters['provider_credit_reversal_details'] - [array(array())]
    # @optional requestParameters['seller_refund_note'] [String]
    # @optional requestParameters['soft_descriptor'] - [String]
    # @optional requestParameters['mws_auth_token'] - [String]

    def refund(requestParameters = {})
        parameters           = {}
        parameters['Action'] = 'RefundPayment'
        parameters["Version"] = "2013-01-01"
        #requestParameters    = array_change_key_case(requestParameters, CASE_LOWER)
        requestKeyChangedParameters = requestParameters.inject({}) do |requestParameters, keys|
		 	requestParameters[keys[0].downcase] = keys[1]
			requestParameters
		end

        fieldMappings = {
            'merchant_id' => 'SellerId',
            'amazon_transaction_id' => 'AmazonTransactionId',
            'amazon_transaction_type' => 'AmazonTransactionIdType',
            'refund_reference_id' => 'RefundReferenceId',
            'refund_amount' => 'RefundAmount.Amount',
            'currency_code' => 'RefundAmount.CurrencyCode',
            'seller_refund_note' => 'SellerRefundNote',
            'soft_descriptor' => 'SoftDescriptor',
            'content_type' => 'ContentType'
        }
        responseObject = setParametersAndPost(parameters, fieldMappings, requestKeyChangedParameters)
        return responseObject
    end

    # GetRefundDetails API call - Returns the status of a particular refund.
    # @see http://docs.developer.amazonservices.com/en_US/off_amazon_payments/OffAmazonPayments_GetRefundDetails.html
    #
    # @param requestParameters['merchant_id'] - [String]
    # @param requestParameters['amazon_refund_id'] - [String]
    # @optional requestParameters['mws_auth_token'] - [String]
    def getRefundDetails(requestParameters = {})
        parameters   		 = {}
        parameters['Action'] = 'GetRefundDetails'
        requestKeyChangedParameters = requestParameters.inject({}) do |requestParameters, keys|
		 	requestParameters[keys[0].downcase] = keys[1]
			requestParameters
		end

        fieldMappings = {
            'merchant_id'         => 'SellerId',
            'amazon_refund_id'  => 'AmazonRefundId',
            'mws_auth_token'     => 'MWSAuthToken'
        }
        responseObject = setParametersAndPost(parameters, fieldMappings, requestKeyChangedParameters)
        return responseObject
    end

	# To get process payment Url with given queryparameters.
	#
	# @param $queryparamaeters
	# @return processPaymentUrl
	def getProcessPaymentUrlWithQueryParameters(queryParameters, redirectUrl)
		validateNotNull(queryParameters, "Query Parameters" )
		validateNotNull(redirectUrl, "Redirect Url" )
		return constructPaymentUrl(queryParameters, redirectUrl )
	end

	def constructPaymentUrl(queryParameters, redirectUrl )
		baseUrl = getBaseUrl()
		redirectUrl = CGI.escape(redirectUrl).gsub("+", "%20")
		processPaymentUrl = "#{baseUrl}/initiatePayment?#{queryParameters}&redirectUrl=#{redirectUrl}"
		return processPaymentUrl;
	end



	# @param string K Key encryption key
	# @param string IV Initialization vector
	# @param null|string P Data to encrypt (null for authentication)
	# @param null|string A Additional Authentication Data
	# @param int tag_length Tag length
	# @return array

	def encrypt(k, iv, p = nil, a = nil, tag_length = 128)
		key = k;
		key_length =key.bytes.length*8 #mb_strlen ( k, '8bit' ) * 8;
		return encryptWithAesGcm( k, key_length, iv, p, a, tag_length )

	end

	# This method will append the tag at the end of the ciphertext.
	# @param string K Key encryption key
	# @param string IV Initialization vector
	# @param null|string P Data to encrypt (null for authentication)
	# @param null|string A Additional Authentication Data
	# @param int tag_length Tag length
	# @return string
	def encryptAndAppendTag(k, iv, p = nil, a = nil, tag_length = 128)
		return encrypt(k, iv, p, a, tag_length).join()
	end

	def getSecureRandomKey()
		return OpenSSL::Random.random_bytes(16)
	end

	def getSecureRandomIv()
		return OpenSSL::Random.random_bytes(12)
	end

	# get public key
	def getPublicKey()
		require 'yaml'
		path= File.dirname(__FILE__)
		config = YAML.load_file(path.concat("/config.yaml"))
		key = getPublicKeyFromFile(config['publicKey'])
		return "-----BEGIN PUBLIC KEY-----\n".concat(key).concat("\n-----END PUBLIC KEY-----")
	end

	def getPublicKeyFromFile(text,max_width = 64)
	  (text.length < max_width) ?
	    text :
	    text.scan(/.{1,#{max_width}}/).join("\n")
	end

	# @param string K Key encryption key
	# @param string key_length Key length
	# @param string IV Initialization vector
	# @param null|string P Data to encrypt (null for authentication)
	# @param null|string A Additional Authentication Data
	# @param int tag_length Tag length
	# @return array
	def encryptWithAesGcm(k, key_length, iv, p = nil, a = nil, tag_length = 128)
		mode = "aes-#{key_length}-gcm"
		tag = nil
		c = true
		cipher = OpenSSL::Cipher.new(mode)
		#cipher = OpenSSL::Cipher::AES.new(key_length, :GCM)
		#cipher.iv_len = 16
		cipher.padding= 0
		cipher.encrypt
		cipher.key = k
		cipher.iv = iv
		c = cipher.update(p) + cipher.final
		tag = cipher.auth_tag
		return [c,tag];
	end

	def addDefaultParameters(parameters)
		parameters ['AWSAccessKeyId'] = @@config['access_key']
		parameters ['SignatureMethod'] = 'HmacSHA256'
		parameters ['SignatureVersion'] = '2'
		return parameters
	end

	def addParametersForEncryption(parameters)
		parameters["sellerId"] = @@config["merchant_id"]
		parameters["startTime"] = Time.now.to_i.to_s
		return parameters
	end # end addParametersForEncryption method

	# This method calculates the signature for the parameters given with the
	# aws secret key provided.
	# @param $parameters the
	# parameters to be signed
	# @return the signature after signing the parameers

	def calculateSignForEncryption(parameters = {})
		validateNotEmpty( parameters, "Parameters" )

		parameters = addParametersForEncryption( parameters )
		@serviceUrl = 'amazonpay.amazon.in'
		@urlScheme = 'POST'
		@path = '/'
		return signParameters( parameters )
	end #end calculateSignForEncryption method

	def calculateSignForVerification(parameters = {})
		validateNotEmpty( parameters, "parameters" )
		@serviceUrl = 'amazonpay.amazon.in'
		@urlScheme = 'POST'
		@path = '/'
		return signParameters( parameters )
	end

	# This method return signature after signing the parameters with the given secret key.
	# @param $parameters the parameters to be signed
	# @return the calculated signature

	def signParameters(parameters = {})
		parameters = urlEncodeParams( parameters )
		parameters = addDefaultParameters( parameters )
		parameters = Hash[parameters.sort_by{|key,val| key.to_s}]
		#parameters = parameters.sort_by{ |key, val| key.to_s }.to_h
		stringToSign = calculateStringToSignV2(parameters )
		sign = sign( stringToSign, @@config ['secret_key'] )
		parameters ['Signature'] = sign
		return parameters
	end # end signParameters method

	# @param $parameters the
	# request parameters to be signed
	# @return String to be signed
	def calculateStringToSignV2( parameters)
		data = @urlScheme
		data.concat("\n")
		data.concat(@serviceUrl)
		data.concat("\n")
		data.concat(@path)
		data.concat("\n")
		data.concat( getParametersAsString( parameters ) )
		return data
	end

	def getParametersToEncrypted(parameters)
		parameters = urlEncodeParams( parameters )
		parameters.delete('SignatureMethod')
		parameters.delete('SignatureVersion')
		return parameters;
	end

	def urlEncodeParams(parameters)
		parameters.each_pair do |key, value|
			parameters[key] = urlEncode(value, false)
		end
		return parameters
	end #end urlEncodeParams method

	def urlEncode(value, path)
		encodedString = value
		if(! value.nil? && ! value.to_s.empty?)
			encodedString = CGI.escape(value.encode( 'UTF-8', 'Windows-1252' )).gsub("+", "%20").gsub("\\", "")
			if (path)
				encodedString = encodedString.gsub('%2F', '/')
			end
		end
		return encodedString
	end #end urlEncode method

	def getParametersAsString(parameters)
		queryParameters = []
		parameters.to_h.each_pair do |key,value|
			if value.nil?
				value = ''
			end
			queryParameters.push( urlEncode(key, false ).concat('=').concat(value) )
		end
		return queryParameters.join('&')
	end

	# Computes RFC 2104-compliant HMAC signature.
	# For more details refer this
	# http://docs.aws.amazon.com/general/latest/gr/signature-version-2.html
	# @param $data the final string to be signed
	# @param $secretKey the secret key to be used for signing the request data
	# @return the final signed string

	def sign(data, secretKey)
		return Base64.strict_encode64(OpenSSL::HMAC.digest('sha256', secretKey, data))
	end

	# check required config params
    def checkConfigKeys(configArray)
    	configArray.each_pair do |key,value|
		    if @@config.has_key?(key)
		    	@@config[key] = value
		    else
		      raise "Key : #{key} is either not part of the configuration or has incorrect Key name.
				check the config array key names to match your key names of your config array"
		    end
		end
    end # end checkConfigKeys mehod

    # check required parameter
    def checkForRequiredParameters(parameters, fields)
    	fields.each do |fieldName, mandatoryField|
    		if mandatoryField
    			value = getMandatoryField( fieldName, parameters )
    		else
    			value = getField( fieldName, parameters )
    		end
    	end
    	parameters.each do |key, value|
    		if ! fields.has_key?(key)
    			raise "Error with json message - provided field #{key} should not be part of input"
    		end
    	end
	end #end checkForRequiredParameters method

	# Extract the mandatory field from the message and return the contents
	# @param string $fieldName
	# name of the field to extract
	# @throws Exception if not found
	# @return string field contents if found
	def getMandatoryField(fieldName, parameters)
		value = getField(fieldName, parameters )
		if value.nil? || value.to_s.empty?
			raise "Error with json message - mandatory field #{fieldName} cannot be found or is empty"
		end
		return value
	end #end getMandatoryField method

	# Extract the field if present, return null if not defined
	# @param string $fieldName
	# name of the field to extract
	# @return string field contents if found, null otherwise
	def getField(fieldName, parameters)

		if parameters.has_key?(fieldName)
			return parameters[fieldName]
		else
			return nil;
		end
	end # end getField method


	#To get process payment Url with given parameters.
	#Calculates signed and encrypted payload and generates url
	#@param $paramaeters to be signed and encrypted
	#@param $redirectUrl
	#@return processPaymentUrl
	def getProcessPaymentUrl(parameters, redirectUrl)

		validateNotNull( redirectUrl, "Redirect Url" )
		validateNotNull( URI.parse(redirectUrl).scheme, "Invalid redirect URL. Please remember to input http:// or https:// as well. URL scheme" )

		queryParameters = generateSignatureAndEncrypt( parameters )
		return constructPaymentUrl(queryParameters, redirectUrl )

	end # end getProcessPaymentUrl method

	#To get process payment Url with given parameters.
	#Calculates signed and encrypted payload and generates url
	#@param $parameters to be signed and encrypted
	#@return payload
	def signAndEncryptParameters(parameters)

		payload = generateSignatureAndEncrypt( parameters )
		return payload

	end # end getProcessPaymentUrl method

	def getBaseUrl()
		baseUrl = getBaseUrlDynamically()
		if baseUrl.nil?
			baseUrl = @@config['base_url']
		end
		return baseUrl
	end

	#TODO
	def getBaseUrlDynamically()
		return nil
	end

	def setParametersAndPost(parameters, fieldMappings, requestParameters)
    	#For loop to take all the non empty parameters in the $requestParameters and add it into the $parameters array,
        #if the keys are matched from $requestParameters array with the $fieldMappings array
        requestParameters.each_pair do |param, value|
        	if  !value.is_a?(Hash)
        		value = value.strip
        	end
        	if fieldMappings.has_key?(param) && value !=''
            	# For variables that are boolean values, strtolower them
        		if checkIfBool(value)
        			value = value.downcase
        		end
        		parameters[fieldMappings[param]] = value
        	end
    	end
    	parameters = setDefaultValues(parameters, fieldMappings, requestParameters)
    	responseObject = calculateSignatureAndPost(parameters)
    	return responseObject
    end

    # If merchant_id is not set via the requestParameters array then it's taken from the config array
    # Set the platform_id if set in the config['platform_id'] array
    # If currency_code is set in the $requestParameters and it exists in the $fieldMappings array, strtoupper it
    # else take the value from config array if set

    def setDefaultValues(parameters, fieldMappings, requestParameters)
        if requestParameters['merchant_id'].to_s.empty?
            parameters['SellerId'] = @@config['merchant_id']
        end
        if  fieldMappings.has_key?('platform_id')
        	if requestParameters['platform_id'].to_s.empty? && !@@config['platform_id'].to_s.empty?
            	parameters[fieldMappings['platform_id']] = @@config['platform_id']
            end
    	end
        if fieldMappings.has_key?('currency_code')
            if ! requestParameters['currency_code'].to_s.empty?
        		parameters[fieldMappings['currency_code']]  = requestParameters['currency_code'].to_s.upcase
            else
                parameters[fieldMappings['currency_code']] = @@config['currency_code'].to_s.upcase
            end
        end
        return parameters
    end

    # calculateSignatureAndPost - convert the Parameters array to string and curl POST the parameters to MWS
    def calculateSignatureAndPost(parameters)
    	# Call the signature and Post function to perform the actions. Returns XML in array format
        parametersString = calculateSignatureAndParametersToString(parameters)
    	# POST using curl the String converted Parameters
    	response = invokePost(parametersString)
        return response
    end

    def calculateSignatureAndParametersToString(parameters = {})
        parameters['Timestamp'] = getFormattedTimestamp()
        createServiceUrl()
        parameters = signParameters(parameters)
        parameters['Signature'] = urlEncode(parameters['Signature'], false)
        parameters = getParametersAsString(parameters)
        return parameters
    end

	# invokePost takes the parameters and invokes the httpPost function to POST the parameters
    # Exponential retries on error 500 and 503
    # The response from the POST is an XML which is converted to Array

    def invokePost(parameters)
        response       = {}
        statusCode     = 200
        @success = false
    	# Submit the request and read response body
    	#try {
            shouldRetry = true
            retries     = 0
            loop do
               # try {
                    constructUserAgentHeader()
                	httpCurlRequest = HttpCurl.new(@@config)
                	url =  @mwsServiceUrl.concat('?').concat(parameters)
                  	response = httpCurlRequest.httpGet(url, @userAgent)

                    statusCode = response['status_code']
            		if statusCode == 200
                        shouldRetry    = false
                        @success = true
                    elsif statusCode == 500 || statusCode == 503
            				shouldRetry = true
                        if shouldRetry && @@config['handle_throttle'].downcase
                        	statusCode = statusCode
                            pauseOnRetry(++retries, statusCode)
                        end
                    else
                        shouldRetry = false
                    end
              #  } catch (\Exception $e) {
              #      throw $e;
             #   }
            	break if shouldRetry == false
			end
        #} catch (\Exception $se) {
        #    throw $se;
        #}
        return response
    end #

	# Exponential sleep on failed request
	# @param retries current retry
	# @throws Exception if maximum number of retries has been reached

	def pauseOnRetry(retries, status)
		max_error_retry = 5
		if (retries <= max_error_retry)
			delay = (( 4 ** retries.to_f ) /10).to_f
			sleep(delay)
		else
			raise "Error Code: #{status} Maximum number of retry attempts - #{retries} reached"
        end
    end

	# Create the User Agent Header sent with the POST request
    def constructUserAgentHeader()
        @userAgent = quoteApplicationName(@@config['application_name']).concat('/').concat(quoteApplicationVersion(@@config['application_version']))
        @userAgent.concat(' (')
        @userAgent.concat('Language=ruby/').concat(RUBY_VERSION)
        @userAgent.concat('; ')
        #@userAgent.concat('Platform=' . php_uname('s') . '/' . php_uname('m') . '/' . php_uname('r');
        #@userAgent.concat('; ')
        @userAgent.concat('MWSClientVersion=2.1.0')
        @userAgent.concat(')')
    end

	# Collapse multiple whitespace characters into a single ' ' and backslash escape '\',
	# and '/' characters from a string.
	# @param $s
	# @return string

	def quoteApplicationName(s)
		quotedString = s.to_s.gsub('/ {2,}|\s/', ' ') #preg_replace('/ {2,}|\s/', ' ', $s);
		quotedString = quotedString.to_s.gsub('/\\\\/', '\\\\\\\\') #preg_replace('/\\\\/', '\\\\\\\\', $quotedString);
		quotedString = quotedString.to_s.gsub('/\//', '\\/') #preg_replace('/\//', '\\/', $quotedString);
		return quotedString
	end
	# Collapse multiple whitespace characters into a single ' ' and backslash escape '\',
	# and '(' characters from a string.
	# @param $s
	# @return string
	def quoteApplicationVersion(s)
		quotedString = s.to_s.gsub('/ {2,}|\s/', ' ') #preg_replace('/ {2,}|\s/', ' ', $s);
		quotedString = quotedString.to_s.gsub('/\\\\/', '\\\\\\\\') #preg_replace('/\\\\/', '\\\\\\\\', $quotedString);
		quotedString = quotedString.to_s.gsub('/\\(/', '\\(') #preg_replace('/\\(/', '\\(', $quotedString);
		return quotedString
	end

	# Create MWS service URL and the Endpoint path
    def createServiceUrl()

				@modePath = 'OffAmazonPayments'
				if @@config['sandbox'] == 'true'
				  @modePath = 'OffAmazonPayments_Sandbox'
				elsif @@config['sandbox'] == 'false'
				  @modePath = 'OffAmazonPayments'
				end
        @serviceUrl  = "mws.amazonservices.in"
        @mwsServiceUrl   = "https://#{@serviceUrl}/#{@modePath}/2013-01-01"
        @path = "/#{@modePath}/2013-01-01"
        @urlScheme = 'GET'
    end

	def getFormattedTimestamp()
		return Time.now.utc.iso8601

    end

	# checkIfBool - checks if the input is a boolean
    def checkIfBool(string)
    	string = string.downcase
    	return ['true', 'false'].include?(string)
    end
	# check value is null or not
	def validateNotNull(value, message)
		if value.nil? || value.to_s.empty?
			raise "#{message} cannot be null."
		end
	end #end validateNotNull method

	def validateNotEmpty(value, message)
		if value.to_s.empty?
			raise "#{message} cannot be empty."
		end
	end # end validateNotEmpty method

 # Helpers to parse the MWS API response
	def to_xml(body)
		REXML::Document.new(body)
	end

	def get_element(xml,xpath, xml_element)
		# xml = xml.to_xml
		xml.elements.each(xpath) do |element|
			@value = nil
			if !element.elements[xml_element].nil?
				@value = element.elements[xml_element].text
			end
		end
		return @value
	end

	def success(code)
      if code.eql? '200'
         return true
      else
         return false
      end
     end

    def listOrderReference(requestParameters = {})
        parameters           = {}
        parameters["Version"] = "2013-01-01"
        parameters['Action'] = 'ListOrderReference'
        parameters["QueryIdType"] = "SellerOrderId"
		parameters["PaymentDomain"] =  "IN_INR"

        requestKeyChangedParameters = requestParameters.inject({}) do |requestParameters, keys|
	 	requestParameters[keys[0].downcase] = keys[1]
		requestParameters
		end
		# add status filters
		filters = requestKeyChangedParameters["orderreferencestatuslistfilter"]
		if filters.is_a?(Array)
			i = 1
			filters.each do |filter|
				parameters["OrderReferenceStatusListFilter.OrderReferenceStatus.#{i}"] = filter
				i+=1
			end
		end
		requestKeyChangedParameters.delete("orderreferencestatuslistfilter")

        fieldMappings = {
            'merchant_id' => 'SellerId',
            'queryid' => 'QueryId',
            'createdtimerangestart' => 'CreatedTimeRange.StartTime',
	        'createdtimerangeend' => 'CreatedTimeRange.EndTime',
            'StartTime' => 'StartTime',
            'EndTime' => 'EndTime',
            'pagesize' => 'PageSize',
            'sortorder' => 'SortOrder'
        }
        responseObject = setParametersAndPost(parameters, fieldMappings, requestKeyChangedParameters)
        return responseObject
    end

    def listOrderReferenceByNextToken(requestParameters = {})

        parameters   		 = {}
        parameters["Version"] = "2013-01-01"
        parameters['Action'] = 'ListOrderReferenceByNextToken'
        requestKeyChangedParameters = requestParameters.inject({}) do |requestParameters, keys|
	 	requestParameters[keys[0].downcase] = keys[1]
		requestParameters
		end

        fieldMappings = {
            'merchant_id'         => 'SellerId',
            'nextpagetoken'  => 'NextPageToken'
        }
        responseObject = setParametersAndPost(parameters, fieldMappings, requestKeyChangedParameters)
        return responseObject
    end
end
