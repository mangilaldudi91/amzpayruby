#!/usr/bin/ruby

require "net/https"
require "json"
class HttpCurl

	@@config = {}
	@@header = false
    @@accessToken = nil
    @@curlResponseInfo = nil

	# Takes user configuration array as input
    # Takes configuration for API call or IPN config
	def initialize(configArray = {})
		@config = configArray
	end

	#Setter for boolean header to get the user info
    def setHttpHeader()
    	@@header = true
    end

    #Setter for Access token to get the user info
    def setAccessToken(accesstoken)
        @@accessToken = accesstoken
  	end

  	# Get the output of Curl Getinfo
    def getCurlResponseInfo()
        return @@curlResponseInfo
    end

    # get request
    def httpGet(url, user_agent, limit = 10)
		raise 'too many HTTP redirects' if limit == 0

		uri = URI.parse(url)
		http = Net::HTTP.new(uri.host, uri.port)

		#request = Net::HTTP::Get.new(uri.request_uri)

		response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
		  	request = Net::HTTP::Get.new(uri)
		  	if !user_agent.empty?
				request["User-Agent"] = user_agent
			end
			request["Accept"] = "*/*"
			request["content-type"] ="text/html; charset=UTF-8"
			http.request(request)
		end

		case response
	  	when Net::HTTPSuccess then
	  		response.each_header do |key, value|
			  #p "#{key} => #{value}"
			end
	  		puts response.code
			#puts response.body
	  	when Net::HTTPRedirection then
	    	location = response['location']
	    	#redirected to #{location}
	    	puts response.code
			puts response["location"]
	    	httpGet(location, user_agent, limit - 1)
	  	else
	    	puts response.code
			puts response.body
			puts response["location"]
	  	end
	  	return {"status_code"=> response.code, "response" => response.body}
	end#end httpGet method

	def httpPost(url, user_agent, limit = 5)
		raise 'too many HTTP redirects' if limit == 0

		uri = URI.parse(url)

		http = Net::HTTP.new(uri.host, uri.port)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_PEER
		#http.use_ssl = (uri.port == 443)

		request = Net::HTTP::Get.new(uri.request_uri)
	  	#request.set_form_data({"q" => "My query", "per_page" => "50"})
	  	if !user_agent.empty?
			request["User-Agent"] = user_agent
		end
		request["Accept"] = "*/*"
		request["Content-Type"] = "application/json"
		response = http.request(request)

		case response
	  	when Net::HTTPSuccess then
	  		response.each_header do |key, value|
			 # p "#{key} => #{value}"
			end
			response.each_pair do |key, value|
			  p "#{key} => #{value}"
			end
	  		puts response.code
			#puts response.body
	  	when Net::HTTPRedirection then
	    	location = response['location']
	    	#redirected to #{location}
	    	puts response.code
			puts response["location"]
	    	httpPost(location, user_agent, limit - 1)
	  	else
	    	puts response.code
			puts response.body
			puts response["location"]
	  	end
	  	return {"status_code"=> response.code, "response" => JSON.parse(response.body)}
	end#end httpPost  method
end


# http = HttpCurl.new()
# url = "https://amazonpay.amazon.in/getDynamicConfig?key=serverSideSDKPHP"
# user_agent = {"User-Agent" => "My Ruby Script"}
# #response = http.httpGet(url, user_agent)
# #http.httpPost(url, user_agent)
# response ={}
