#!/usr/bin/ruby


require 'yaml'
require "json"
require_relative './HttpCurl'

module AmazonPay

	def getDynamicConfig()
		config = YAML.load_file("./config.yaml")
		if( config['lastFetchedTimeForFetchingConfig']<(Time.now.to_i-3600) )
			httpCurlRequest = HttpCurl.new()
			dynamicConfig = httpCurlRequest.httpGet('https://amazonpay.amazon.in/getDynamicConfig?key=serverSideSDKPHP', '')
			response = JSON.parse(dynamicConfig['response'])
			if  dynamicConfig.is_a?(Hash) && dynamicConfig['status_code'] == '200' && !response['publicKey'].empty?
				config['publicKey'] = response['publicKey']
				config['lastFetchedTimeForFetchingConfig'] = Time.now.to_i
				File.open('./config.yaml', 'w') {|f| f.write(config.to_yaml) }
			else
				raise "Public key is not present"
			end
		end
	end
end
getDynamicConfig()
